import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './index.css';
import './App.css';
import Gallery from './components/gallery';
import Projector from './components/projector';
import registerServiceWorker from './registerServiceWorker';

const dimensions = { projector: {width: 1920, height: 1080}}

ReactDOM.render(
    <Router>
        <div>
            <Route exact path="/" component={Gallery} />
            <Route path="/projector"
                render={(props) => <Projector {...props} dimensions={dimensions} /> } />
        </div>
    </Router>,
    document.getElementById('root'));

registerServiceWorker();
