import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './App.css';
import Gallery from './components/gallery';
import Projector from './components/projector';

class App extends Component {
    render() {
        return (
            <div className="App">
                <Gallery />
            </div>
        );
    }
}

export default App;
