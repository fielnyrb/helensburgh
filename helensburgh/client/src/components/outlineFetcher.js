//Returns a set of outlines from a source

const OutlineFetcher = {
    getOutlines: () => {
        return [
            [ //image start
                {//outline start
                    id: 1,
                    path: [
                        { x: 109, y: 259 },
                        { x: 222, y: 292 },
                        { x: 315, y: 292 },
                        { x: 347, y: 397 },
                        { x: 79, y: 441 }
                    ],
                    center: {x: 450, y: 1079}
                },
                {
                    id: 2,
                    path: [
                        { x: 450, y: 222 },
                        { x: 459, y: 166 },
                        { x: 572, y: 125 },
                        { x: 667, y: 112 },
                        { x: 688, y: 268 },
                        { x: 425, y: 279 },
                    ],
                    center: { x: 1920, y: 500 }
                }
            ],
            [
                {
                    id: 3,
                    path: [
                        { x: 273, y: 225 },
                        { x: 402, y: 223 },
                        { x: 404, y: 259 },
                        { x: 283, y: 258 },
                    ],
                    center: { x: 884, y: 658 }
                }
            ]
        ];
    }
}

export default OutlineFetcher;