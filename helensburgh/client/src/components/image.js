//Class Image
//Shows an image and is capable of displaying multiple selections on top of the image

import React from 'react';

const Image = (props) => {

    const backgroundImage = {
        backgroundImage: 'url(' + props.selectedImage + ')',
    };

    function getOutlines() {
        if (!props.outlines) {
            return '';
        }
        var path = props.outlines;

        //constructs n <path> tags that are ready to be put into a set of <svg> tags
        path = path.map((element, i) => {
            const p = element.path.map((e, index) => {
                const command = index === 0 ? 'M' : 'L';
                return command + e.x + ' ' + e.y;
            }).join(' ');

            return <path key={i} d={p + ' Z'} onClick={() => props.onOutlineTriggered(i)} />;
        });

        return path;
    }


    return (
        <svg className="paintShapes" style={backgroundImage}>
            {getOutlines()}
        </svg>
    );
}

export default Image;