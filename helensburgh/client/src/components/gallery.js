import React, { Component } from 'react';
import Image from './image';
import Showcase from './showcase';
import Transmitter from './transmitter';
import ResourceFetcher from './resourceFetcher';

class Gallery extends Component {

    constructor() {
        super();
        this.transmitter = new Transmitter();
    }

    state = {
        selectedImage: null,
        selectedOutline: null
    };

    selectImage(imageId) {
        const resources = ResourceFetcher();
        const selectedResource = resources.getResources()[imageId];

        this.setState({
            selectedImage: selectedResource.image.image,
            selectedOutline: selectedResource.outlines
        });

        this.transmitter.transmit(imageId, this.transmitter.IMAGE_CHANGE);
    }

    triggerOutline(outlineId) {
        this.transmitter.transmit(outlineId, this.transmitter.OUTLINE_TRIGGER)
    }

    render() {
        return (
            <div>
                <div>
                    <Image
                        selectedImage={this.state.selectedImage}
                        outlines={this.state.selectedOutline}
                        onOutlineTriggered={this.triggerOutline.bind(this)}
                    />
                </div>
                <button onClick={() => this.triggerOutline(-1)}>Reset</button>
                <div>
                    <Showcase onSelectImage={this.selectImage.bind(this)} />
                </div>
            </div>
        );
    }
}

export default Gallery;