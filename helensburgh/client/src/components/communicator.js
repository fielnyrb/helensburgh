import OpenSocket from 'socket.io-client';

const serverIP = 'localhost';

class Communicator {
    constructor() {
        this.IMAGE_CHANGE = 10001;
        this.OUTLINE_TRIGGER = 10002;
        this.socket = OpenSocket('http://' + serverIP + ':8000');
    }
};

export default Communicator;