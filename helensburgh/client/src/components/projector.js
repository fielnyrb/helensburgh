import React, { Component } from 'react';
import Receiver from './receiver';
import ResourceFetcher from './resourceFetcher';
import Zoomer from './zoomer';

class Projector extends Component {
    constructor(props) {
        super(props);
        this.setListeners();
        this.zoomer = new Zoomer();
        this.zoomer.setImageSize(props.dimensions.projector.width, props.dimensions.projector.height);
    }

    state = {
        imageId: null, //Id from server
        image: null,
        zoomedIn: false
    }

    setListeners() {
        const resources = ResourceFetcher();
        const receiver = new Receiver();

        //Listen for image_change
        receiver.listen(imageId => {
            this.setState({
                imageId: imageId,
                image: resources.getResources()[imageId].image.image
            });
        }, receiver.IMAGE_CHANGE);

        //Listen for outline trigger
        receiver.listen(outlineId => {
            if (outlineId === -1) {
                this.setState({ zoomedIn: false });
                return;
            }

            var outline = resources.getResources()[this.state.imageId].outlines;
            this.zoomer.setCoordinates(outline[outlineId].center.x, outline[outlineId].center.y);
            this.setState({ zoomedIn: true });
        }, receiver.OUTLINE_TRIGGER);
    }

    render() {
        const zoomLevel = this.state.zoomedIn ? this.zoomer.STANDARD_ZOOM : this.zoomer.DEFAULT_ZOOM;
        const imageStyle = this.zoomer.zoom(zoomLevel, this.state.image);

        return <div className="projectorImage" style={imageStyle}>
        </div>;
    }

}

export default Projector;