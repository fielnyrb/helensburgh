//Used by the projector screen to enlarge the image to simulate a zoom into input coordinates
//Zoomer handles css-style elements


class Zoomer {
    constructor() {
        this.DEFAULT_ZOOM = 10001;
        this.STANDARD_ZOOM = 10002;
        this.coordinates = {};
        this.screenSize = {};
    }

    setImageSize(width, height) {
        this.sceenSize = { width, height };
    }

    setCoordinates(x, y) {
        this.coordinates = { x, y };
    }

    getTransformOriginAsString() {
        const x = Math.floor((this.coordinates.x / this.sceenSize.width) * 100);
        const y = Math.floor((this.coordinates.y / this.sceenSize.height) * 100);
        return `${x}% ${y}%`;
    }

    zoom(setting, img) {
        switch (setting) {
            case this.DEFAULT_ZOOM:
                return {
                    backgroundImage: 'url(' + img + ')',
                    transition: 'transform 2s',
                    transform: 'scale(1.0)',
                    transformOrigin: this.getTransformOriginAsString()
                };
            case this.STANDARD_ZOOM:
                return {
                    backgroundImage: 'url(' + img + ')',
                    transition: 'transform 2s',
                    transform: 'scale(2.5)',
                    transformOrigin: this.getTransformOriginAsString()
                }
            default:
                break;
        }
        return {};
    }
}

export default Zoomer;