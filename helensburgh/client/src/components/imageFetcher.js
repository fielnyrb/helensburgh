import image1 from '../images/image1.jpg';
import image2 from '../images/image2.jpg';

const ImageFetcher =  {
    getImages : () => {
        return [
            {id:1, image: image1 },
            {id:2, image: image2 }
        ];
    }
}

export default ImageFetcher;