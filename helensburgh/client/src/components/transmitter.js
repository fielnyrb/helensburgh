import Communicator from './communicator';

class Transmitter extends Communicator {
    transmit(id, transmissionType) {
        switch (transmissionType) {
            case this.IMAGE_CHANGE:
                this.socket.emit('imageChangeFromControlPanel', id);
                break;
            case this.OUTLINE_TRIGGER:
                this.socket.emit('outlineTriggerFromControlPanel', id);
                break;
            default:
        }
    }
}

export default Transmitter;