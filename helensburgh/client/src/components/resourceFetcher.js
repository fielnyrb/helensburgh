import ImageFetcher from './imageFetcher';
import OutlineFetcher from './outlineFetcher';

const ResourceFetcher = () => {
    function mergeImagesAndOutlines() {
        const images = ImageFetcher.getImages();
        const outlines = OutlineFetcher.getOutlines();

        return images.map((e, index) => {
            return { outlines : outlines[index], image:e };
        });
    }

    return {
        getResources: () => {
            return mergeImagesAndOutlines();
        }
    }
};

export default ResourceFetcher;