import React from 'react';
import ImageFetcher from './imageFetcher';

const Showcase = (props) => {
    function getImageList() {
        return images.map((e, index, array) => {
            return <li key={index} className="list-inline-item" onClick={() => props.onSelectImage(index)}>
                <img alt="" src={e.image} height="100px" width="100px" />
            </li>
        });
    }

    const images = ImageFetcher.getImages();

    return <div>
        <ul className="list-inline">
            {getImageList()}
        </ul>
    </div>;
}

export default Showcase;