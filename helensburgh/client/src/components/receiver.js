import Communicator from './communicator';

class Receiver extends Communicator {
    listen(callback, listeningType) {
        switch (listeningType) {
            case this.IMAGE_CHANGE:
                this.socket.on('imageChangeToProjector', imageId => callback(imageId));
                break;
            case this.OUTLINE_TRIGGER:
                this.socket.on('outlineTriggerToProjector', outlineId => callback(outlineId));
                break;
            default:
        }
    }
}

export default Receiver;