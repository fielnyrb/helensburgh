const io = require('socket.io')();

const port = 8000;

io.on('connection', (client) => {
    client.on('imageChangeFromControlPanel', (data) => {
        io.emit('imageChangeToProjector', data);
    });
    client.on('outlineTriggerFromControlPanel', (data) => {
        io.emit('outlineTriggerToProjector', data);
    });
});

io.listen(port);
console.log('Listening on port', port);